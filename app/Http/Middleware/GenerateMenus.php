<?php

namespace App\Http\Middleware;


use App\Modules\Backend\BackEndMenu;
use Closure;
use Illuminate\View\View;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        \Menu::make('MyNavBar', function ($menu) {
//            $menu->add('Home',     array('class' => 'nav-item', 'id' => 'home'));
//            $menu->add('About',    array('class' => 'nav-item'));
//            $menu->add('Contact',  'contact');
//        });


        $route = explode('/',url()->full());
        if (count($route)>3){
            if ($route[3]=='admin'){
                view()->share( 'main_menu', BackEndMenu::build_admin_menu());
            };
        };

        return $next($request);
    }
}
