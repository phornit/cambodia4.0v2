<?php

namespace App\Http\Resources;

use http\Client\Curl\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class startupClubCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($page){
                return [
                    'id' => $page->id,
                    'name' => $page->name,
                    'slug' => $page->slug,
                    'state' => $page->state,
                    'created_at' => $page->created_at,
                    'subCategories' => User::where('id', $page->user_id)->get()
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
