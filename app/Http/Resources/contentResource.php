<?php

namespace App\Http\Resources;

use App\Models\Backend\ContentCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class contentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'alias' => $this->alias,
            'thumb_image' => $this->thumb_image,
            'brief' => $this->brief,
            'description' => $this->description,
            'event_date' => $this->event_date,
            'metakey' => $this->metakey,
            'metades' => $this->metades,
            'is_brief' => $this->is_brief,
            'is_footer' => $this->is_footer,
            'is_homepage' => $this->is_homepage,
            'ordering' => $this->ordering,
            'state' => $this->state,
            'created_at' => $this->created_at,
            'categories' => ContentCategory::whereIn('id', explode(',', preg_replace(array('/{/', '/}/'), "", $this->ref_cat_id)))->get()
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
