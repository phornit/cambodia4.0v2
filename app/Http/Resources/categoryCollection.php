<?php

namespace App\Http\Resources;

use App\Models\Backend\ContentCategory;
use Illuminate\Http\Resources\Json\ResourceCollection;

class categoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($page){
                return [
                    'id' => $page->id,
                    'parent_id' => $page->parent_id,
                    'name' => $page->name,
                    'slug' => $page->slug,
                    'alias' => $page->alias,
                    'state' => $page->state,
                    'metakey' => $page->metakey,
                    'metades' => $page->metades,
                    'created_at' => $page->created_at,
                    'subCategories' => ContentCategory::where('parent_id', $page->id)->get()
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
