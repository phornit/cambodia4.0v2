<?php

namespace App\Http\Resources;

use App\Models\Backend\ContentCategory;
use Illuminate\Http\Resources\Json\ResourceCollection;

class contentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($page){
                return [
                    'id' => $page->id,
                    'title' => $page->title,
                    'slug' => $page->slug,
                    'alias' => $page->alias,
                    'thumb_image' => $page->thumb_image,
                    'brief' => $page->brief,
                    'description' => $page->description,
                    'event_date' => $page->event_date,
                    'metakey' => $page->metakey,
                    'metades' => $page->metades,
                    'is_brief' => $page->is_brief,
                    'is_footer' => $page->is_footer,
                    'is_homepage' => $page->is_homepage,
                    'ordering' => $page->ordering,
                    'state' => $page->state,
                    'created_at' => $page->created_at,
                    'categories' => ContentCategory::whereIn('id', explode(',', preg_replace(array('/{/', '/}/'), "", $page->ref_cat_id)))->get()
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
