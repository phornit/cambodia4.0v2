<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CambodiaCenterController extends Controller
{
    public function index()
    {
        return view('frontend/cambodia_center/cambodia_center');
    }
}
