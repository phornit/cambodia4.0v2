<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        return view('frontend/subject/index');
    }

    public function show($id)
    {
        return view('frontend/subject/subject_detail');
    }
}
