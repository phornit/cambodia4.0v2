<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function show($id)
    {
        $data['content'] = Content::find($id);
        return view('frontend/content/content_detail', $data);
    }
}
