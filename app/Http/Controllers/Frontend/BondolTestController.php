<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BondolTestController extends Controller
{
    //
     public function digital_interaction(){

    	return view('bondol_test/index');
    }


    // contact
    public function contact(){

    	return view('bondol_test/contact');
    }

    // News
    public function news(){

    	return view('bondol_test/news');
    }
}
