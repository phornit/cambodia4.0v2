<?php

namespace App\Http\Controllers;

use App\Mail\SendMailable;
use App\Models\Backend\Content;
use App\Modules\Frontend\FrontEndMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
//        $navi = FrontEndMenu::build_nav_menu();
//        view()->share('main_menu', $navi->main_nav);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['content'] = Content::where('trashed', false)->where('is_homepage', true)->orderBy('id', 'desc')->take(8)->get();
        return view('home',$data);
    }

    public function mail()
    {
        $name = 'Krunal';
        Mail::to('phornit@gmail.com')->send(new SendMailable($name));
        return 'Email was sent';
    }
}
