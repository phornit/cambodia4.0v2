<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\StartupClub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class StartupClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = StartupClub::leftJoin("core_user_details", "core_user_details.user_account_id", "tbl_startup_club.user_id")
            ->select("core_user_details.*", "tbl_startup_club.*")
            ->paginate(CoreFunction::config('Pagination'));

        return $this->sendListResponse($data);
    }

    /**
     * Update state to 1 where query string id
     * @param  int  $id
     * @return
     */
    public function approve($id){
        $data = StartupClub::find($id);
        $data->update(['state'=>1]);

        if($data){
            $name = "ក្លឹបធុរកិច្ចថ្មីកម្ពុជា";
            Mail::to($data['email'])->send(new SendMailable($name));
        } 
        return $this->sendResponse($data);
    }
}
