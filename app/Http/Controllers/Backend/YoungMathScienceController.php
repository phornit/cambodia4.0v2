<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\YoungMathScience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class YoungMathScienceController extends Controller
{
    public function index(){
        $data = YoungMathScience::latest()->paginate(CoreFunction::config('Pagination'));
        return $this->sendListResponse($data);
    }

    /**
     * Update state to 1 where query string id
     * @param  int  $id
     * @return
     */
    public function approve($id){

        $data = YoungMathScience::find($id);
        $data->update(['state'=>1]);

        if($data){
            $name = "ក្លឹបអ្នកគណិតវិទ្យា និងអ្នកវិទ្យាសាស្រ្តវ័យក្មេង (ក.គ.វ)";
            Mail::to($data['email_v'])->queue(new SendMailable($name));
            
        }                
        return $this->sendResponse($data);
    }
}
