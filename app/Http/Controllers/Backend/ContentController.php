<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\contentCollection;
use App\Http\Resources\contentResource;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $data = Content::where('trashed', false)->orderBy('id', 'desc')->paginate(CoreFunction::config('Pagination'));
        return new contentCollection($data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'ref_cat_id' => 'required',
            'thumb_image' => 'required',
            'title' => 'required|string',
            'slug' => 'required|string'
        ]);

        $cate = $request['ref_cat_id'];
        foreach ($cate as $val) {
            $arr_cate[] = $val['id'];
        }

        $request['ref_cat_id'] = '{'.implode(',', $arr_cate).'}';
        $request['state'] = (!empty($request['state'])) ? 1 : 0;
        $request['is_brief'] = (!empty($request['is_brief'])) ? 1 : 0;
        $request['is_footer'] = (!empty($request['is_footer'])) ? 1 : 0;
        $request['is_homepage'] = (!empty($request['is_homepage'])) ? 1 : 0;
        $request['ordering'] = 0;
        $request['created_by'] = auth()->user()->id;
        $request['trashed']= false;

        $data = Content::create($request->all());
        return $this->sendResponse($data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|string',
            'slug' => 'required|string'
        ]);

        $cate = $request['categories'];
        foreach ($cate as $val) {
            $arr_cate[] = $val['id'];
        }

        $data = Content::where('id',$id)
            ->update([
                'ref_cat_id' => '{'.implode(',', $arr_cate).'}',
                'title' => $request['title'],
                'slug' => $request['slug'],
                'alias' => $request['alias'],
                'brief' => $request['brief'],
                'description' => $request['description'],
                'thumb_image' => $request['thumb_image'],
                'state' => (!empty($request['state'])) ? 1 : 0,
                'is_brief' => (!empty($request['is_brief'])) ? 1 : 0,
                'is_footer' => (!empty($request['is_footer'])) ? 1 : 0,
                'is_homepage' => (!empty($request['is_homepage'])) ? 1 : 0,
                'metakey' => $request['metakey'],
                'metades' => $request['metades'],
                'created_by' => auth()->user()->id,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

        return $this->sendResponse($data);
    }

    public function show($id)
    {
        $data = Content::find($id);
        return new contentResource($data);
    }

    public function trash($id){
        $data = Content::where('id', $id)
            ->update([
                'trashed' => 1,
                'trashed_by' => auth()->user()->id,
                'trashed_at' => date('Y-m-d H:i:s')
            ]);

        return $this->sendResponse($data);
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $content = Content::where(function($query) use ($search){
                $query->where('title','LIKE',"%$search%");
            })->paginate(CoreFunction::config('Pagination'));
        }else{
            $content = Content::latest()->paginate(CoreFunction::config('Pagination'));
        }

        return $content;

    }
}
