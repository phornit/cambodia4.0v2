<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CKSource\CKFinder\Image;

class FileUploadController extends Controller
{
    public function upload(Request $request) {

        if(!$request->hasFile('image')) {
            return response()->json(['upload_file_not_found'], 400);
        }
        $file = $request->file('image');
        if(!$file->isValid()) {
            return response()->json(['invalid_file_upload'], 400);
        }
        $path = public_path() . '/images/contents/thumbnails/';
        $file->move($path, $file->getClientOriginalName());
        return response()->json(compact('path'));
    }

    public function fileUpload(Request $request)
    {
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('/images/contents/thumbnails'), date('Y-m-d-H-i-s')."-".$imageName);

        return response()->json(['success'=>'You have successfully upload image.']);
    }

    public function resizeImagePost(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file('image');
        $imageName = time().'.'.$image->extension();

        $newImageName = date('Y-m-d-H-i-s')."-".$imageName;

        $destinationPath = 'images/thumbnails';
        $img = \Image::make($image->path());
        $img->resize(414, 414, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$newImageName);

        $destinationPath = 'images/contents/Original';
        $image->move($destinationPath, $newImageName);
        $data = [
            'name' => $newImageName,
            'part_original' => "images/contents/Original/". $newImageName,
            'part_thumbnail' => "images/thumbnails/". $newImageName
        ];
        return $this->sendResponse($data);
    }

    public function removeImage($fileName)
    {
        $path_original = "images/contents/Original/".$fileName;
        $path_thumbnail = "images/thumbnails/".$fileName;

        if(file_exists(public_path($path_original))){
            unlink($path_original);
            unlink($path_thumbnail);
            $data = ['name' => $fileName];
            return $this->sendResponse($data);
        }
    }


    public function saveimages()
    {

        $destinationPath = 'public/images/'; // upload path
        $profileImage = time().'.'.$request->image->getClientOriginalExtension();
        $files->move($destinationPath, $profileImage);

        $data = [
            'name' => $profileImage,
        ];
        return $this->sendResponse($data);

    }

}
