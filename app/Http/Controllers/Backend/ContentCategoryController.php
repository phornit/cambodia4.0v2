<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\categoryCollection;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\ContentCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContentCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ContentCategory::where('parent_id', 0)->orderBy('id','DESC')->paginate(CoreFunction::config('Pagination'));
        return new categoryCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191'
        ]);
        $request['state'] = $request['state'];
        $request['ordering'] = 0;
        $request['created_by'] = auth()->user()->id;
        $request['created_at'] = now()->timestamp;
        $request['trashed']= false;
        $data = ContentCategory::create($request->all());

        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ContentCategory::findOrFail($id);
        return $this->sendResponse($data);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191'
        ]);

        $request['state'] = $request['state'];

        $data = $contentCategory = ContentCategory::where('id', $id)
            ->update($request->all());

        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = ContentCategory::findOrFail($id);
        $data = $role->delete();
        return $this->sendResponse($data);
    }

    public function parents(){
        $data = ContentCategory::where('parent_id', '=', 0)->get();
        return $this->sendResponse($data);
    }

    public function getAllCategories(){
        $data = ContentCategory::latest()->get();
        return $this->sendResponse($data);
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $category = ContentCategory::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%");
            })->paginate(CoreFunction::config('Pagination'));
        }else{
            $category = ContentCategory::latest()->paginate(CoreFunction::config('Pagination'));
        }

        return $category;

    }
}
