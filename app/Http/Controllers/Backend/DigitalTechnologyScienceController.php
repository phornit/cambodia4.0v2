<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\DigitalTechnologyScience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class DigitalTechnologyScienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DigitalTechnologyScience::latest()->paginate(CoreFunction::config('Pagination'));
        return $this->sendListResponse($data);
    }

    /**
     * Update state to 1 where query string id
     * @param  int  $id
     * @return
     */
    public function approve($id){
        $data = DigitalTechnologyScience::find($id);
        $data->update(['state'=>1]);

        if($data){
            $name = "ក្លឹបអ្នកជំនាញបច្ចេកវិទ្យាឌីជីថល និងអ្នកវិទ្យាសាស្រ្ត";
            Mail::to($data['email_v'])->send(new SendMailable($name));
        } 
        return $this->sendResponse($data);
    }
}
