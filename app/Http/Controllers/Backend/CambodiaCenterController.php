<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\CambodiaCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class CambodiaCenterController extends Controller
{
    public function cambodiaCenterLegalEntityList(){
        $data = CambodiaCenter::leftJoin("core_user_details", "core_user_details.user_account_id", "tbl_cambodia4_0_center.user_id")
            ->where("tbl_cambodia4_0_center.type", "BUSINESS")
            ->select("core_user_details.*", "tbl_cambodia4_0_center.*")
            ->paginate(CoreFunction::config('Pagination'));

        return $this->sendListResponse($data);
    }

    public function cambodiaCenterPhysicalPersonList(){
        $data = CambodiaCenter::leftJoin("core_user_details", "core_user_details.user_account_id", "tbl_cambodia4_0_center.user_id")
            ->where("tbl_cambodia4_0_center.type", "PERSONAL")
            ->select("core_user_details.*", "tbl_cambodia4_0_center.*")
            ->paginate(CoreFunction::config('Pagination'));

        return $this->sendListResponse($data);
    }

    /**
     * Update state to 1 where query string id
     * @param  int  $id
     * @return
     */

    public function approve($id){
        $data = CambodiaCenter::find($id);
        $data->update(['state'=>1]);

        if($data){
            $name = "មជ្ឈមណ្ឌលកម្ពុជា ៤.០";
            
            if($data['email']){
                Mail::to($data['email'])->send(new SendMailable($name));
            }else{
                Mail::to($data['email_v'])->send(new SendMailable($name));
            }
        }
        return $this->sendResponse($data);
    }
}
