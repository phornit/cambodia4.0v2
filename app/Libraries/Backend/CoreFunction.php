<?php


namespace App\Libraries\Backend;


use Illuminate\Support\Facades\DB;

class CoreFunction
{
    static function config($name){
        $data = DB::table('configuration_info')
            ->where('is_active','=', '1')
            ->where('name','=', $name)->value('value');
        return $data;
    }

    static function isActive ($id, $state, $name)
    {
        if ($state == 0)
        {
            $state_data = '<button class="btn btn-sm btn-danger" type="button" onclick="window.location.href=\''.URL::current().'/state?id='.$id.'&state=1&name='.$name.'\'"><i class="fa fa-eye-slash"></i></button>';
        }
        else
        {
            $state_data = '<button class="btn btn-sm btn-success" type="button" onclick="window.location.href=\''.URL::current().'/state?id='.$id.'&state=0&name='.$name.'\'"><i class="fa fa-eye"></i></button>';
        }

        return $state_data;
    }

    static function idToArray($id){
        $arr[] = preg_replace(array('/{/', '/}/'), "", $id)->toArray();
        return $arr;
    }

    static function build_created_data($user_id, $date)
    {
        $current_date		=	date('Y-m-d');
        $str_date			=	date('Y-m-d',strtotime($date));

        $user_name = CoreModel::getUserName($user_id);

        if ($user_name)
        {
            if($current_date==$str_date)
            {
                $created_log = date('d-M-Y H:i:s',strtotime($date)).'<br><span class="users">by:&nbsp;'.$user_name.'</span>&nbsp;&nbsp;<img border="0" src="'. url('/backend/img/new.gif') .'">';
            }
            else
            {
                $created_log = date('d-M-Y H:i:s',strtotime($date)).'<br><span class="users">by:&nbsp;'.$user_name.'</span>';
            }

            return $created_log;
        }
    }

    static function build_updated_data($user_id, $date)
    {
        $current_date		=	date('Y-m-d');
        $str_date			=	date('Y-m-d',strtotime($date));

        $user_name = CoreModel::getUserName($user_id);

        if ($user_name)
        {
            if($current_date==$str_date)
            {
                $updated_log = date('d-M-Y H:i:s',strtotime($date)).'<br><span class="users">by:&nbsp;'.$user_name.'</span>&nbsp;&nbsp;<img border="0" src="'. url('/backend/img/updated.gif') .'">';
            }
            else
            {
                $updated_log = date('d-M-Y H:i:s',strtotime($date)).'<br><span class="users">by:&nbsp;'.$user_name.'</span>';
            }

            return $updated_log;
        }
    }
}
