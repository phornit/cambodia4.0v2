<?php


namespace App\Libraries\Backend;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CoreSetting
{
    static function checkSEO ()
    {
        $query = DB::table('tbl_setting');
        $query->where('id', '=', 1);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->seo_url;
        }
    }

    static function display_record()
    {
        $query = DB::table('tbl_setting');
        $query->where('id', '=', 1);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->content_category_display;
        }
    }

    static function getContSLUG($id)
    {
        $query = DB::table('tbl_contents');
        $query->where('id', '=', $id);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->slug;
        }
    }

    static function getContCateSLUG($id)
    {
        $query = DB::table('tbl_content_categories');
        $query->where('id', '=', $id);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->slug;
        }
    }

    static function getContCateParentSLUG($id)
    {
        $query = DB::table('tbl_content_categories');
        $query->where('id', '=', $id);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->slug;
        }
    }

    static function getModSLUG($id)
    {
        $query = DB::table('tbl_modules');
        $query->where('id', '=', $id);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->slug;
        }
    }

    static function ContSEOURL($id, $slug)
    {
        $locale = (Config::get('app.locale') != Config::get('app.fallback_locale')) ? Config::get('app.locale').'/' : '';
        $seo = self::checkSEO();
        if ($seo === 1)
        {
            return url($locale.$slug);
        }
        else if ($seo === 0)
        {
            return url($locale.'cont/'.$id);
        }
    }

    static function ContCateSEOURL($id, $slug)
    {
        $locale = (Config::get('app.locale') != Config::get('app.fallback_locale')) ? Config::get('app.locale').'/' : '';
        $seo = self::checkSEO();
        if ($seo === 1)
        {
            return url($locale.'category/'.$slug);
        }
        else if ($seo === 0)
        {
            return url($locale.'cate/'.$id);
        }
    }

    static function ModSEOURL($id, $slug)
    {
        $locale = (Config::get('app.locale') != Config::get('app.fallback_locale')) ? Config::get('app.locale').'/' : '';
        $seo = self::checkSEO();
        if ($seo === 1)
        {
            return url($locale.'page/'.$slug);
        }
        else if ($seo === 0)
        {
            return url($locale.'mod/'.$id);
        }
    }

    static function ContCateparentSEOURL($id, $slug)
    {
        $locale = (Config::get('app.locale') != Config::get('app.fallback_locale')) ? Config::get('app.locale').'/' : '';
        $seo = self::checkSEO();
        if ($seo === 1)
        {
            return url($locale.'categorysub/'.$slug);
        }
        else if ($seo === 0)
        {
            return url($locale.'cate/'.$id);
        }
    }

    static function buildDate($evendate, $createdate)
    {
        if (!empty($evendate))
        {
            $raw = explode('/', $evendate);
            $dd = $raw[1];
            $mm = $raw[0];
            $yy = $raw[2];

            $date = (object) ['dd' => date($dd), 'mm' => date("M", mktime(0, 0, 0, $mm, 10)), 'yy' => $yy];

            return $date;
        }
        elseif (!empty($createdate))
        {
            $date = explode(' ', $createdate);
            $raw = explode('-', $date[0]);
            $dd = $raw[2];
            $mm = $raw[1];
            $yy = $raw[0];

            $date = (object) ['dd' => date($dd), 'mm' => date("M", mktime(0, 0, 0, $mm, 10)), 'yy' => $yy];

            return $date;
        }
    }

    static function buildContCateName($cont_ids)
    {
        $query = DB::table('tbl_content_categories');
        $query->whereIn('tbl_content_categories.id', explode(',', preg_replace(array('/{/', '/}/'), "", $cont_ids)));
        $results = $query->get();

        $contcate = [];
        foreach ($results as $val)
        {
            $contcate[] = $val->alias ? $val->alias : $val->name;
        }

        return implode(' | ', $contcate);
    }
}
