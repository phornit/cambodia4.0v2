<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class ContentCategory extends Model
{
    protected $table = "tbl_content_categories";

    protected $fillable = ['parent_id', 'name', 'slug', 'alias', 'metakey', 'metades', 'ordering', 'state', 'created_by', 'created_at', 'updated_by', 'updated_at', 'trashed', 'trashed_by', 'trashed_at'];
}
