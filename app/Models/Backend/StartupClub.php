<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class StartupClub extends Model
{
    protected $table = "tbl_startup_club";

    protected $fillable = [
        'company_name',
        'company_type',
        'year_company',
        'total_employee_company',
        'address_company',
        'facebook_name',
        'phone_number_owner',
        'website',
        'representative',
        'position',
        'phone_number',
        'emails',
        'skill',
        'increase_contact',
        'consult',
        'give_advice',
        'support',
        'capital',
        'about_business',
        'other',
        'state',
        'user_role',
        'created_by',
        'created_at'
    ];
}
