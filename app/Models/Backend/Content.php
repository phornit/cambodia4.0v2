<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = "tbl_contents";

    protected $fillable = ['ref_cat_id', 'title', 'slug', 'alias', 'thumb_image', 'brief', 'description', 'event_date', 'metakey', 'metades', 'is_brief', 'is_footer', 'is_homepage', 'ordering', 'state','created_by','trashed', 'trashed_by', 'trashed_at'];

}
