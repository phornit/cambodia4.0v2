<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class CambodiaCenter extends Model
{
    protected $table = "tbl_cambodia4_0_center";

    protected $fillable = [
        'company_name',
        'company_type',
        'year_company',
        'total_employee_company',
        'address_company',
        'facebook_name',
        'phone_number_owner',
        'emails',
        'website',
        'representative',
        'position',
        'skill',
        'state',
        'type',
        'user_role',
        'created_by',
        'created_at',
    ];
}
