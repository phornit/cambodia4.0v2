<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class DigitalTechnologyScience extends Model
{
    protected $table = "tbl_math_t_center";

    protected $fillable = [
        'name_kh_v',
        'name_en_v',
        'sex_v',
        'national_v',
        'dob_v',
        'pob_v',
        'address_v',
        'phone_number_v',
        'email_v',
        'job_v',
        'degree_v',
        'skill_v',
        'work_exper_v',
        'state',
        'user_role',
        'created_by'
    ];
}
