<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    protected $table = "core_user_permission";

    protected $fillable = [
        'user_roles_id',
        'menu_id',
    ];
}
