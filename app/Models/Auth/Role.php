<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'core_user_roles';

    protected $fillable = [
        'id','name','state', 'description','created_by', 'updated_by', 'trashed', 'trashed_by', 'trashed_at'
    ];
}
