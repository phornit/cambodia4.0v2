<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreUsersAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_users_account', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('emails')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('state')->nullable()->default(true);
            $table->integer('user_role');
            $table->boolean('trashed')->nullable()->default(false);
            $table->tinyInteger('trashed_by');
            $table->timestamp('trashed_at')->nullable()->default(time());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_users_account');
    }
}
