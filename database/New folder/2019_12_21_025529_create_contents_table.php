<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref_cat_id');
            $table->string('title');
            $table->string('slug');
            $table->string('alias');
            $table->string('thumb_image');
            $table->text('brief');
            $table->text('description');
            $table->string('event_date');
            $table->string('metakey');
            $table->string('metades');
            $table->boolean('is_brief')->default(false);
            $table->boolean('is_footer')->default(false);
            $table->boolean('is_homepage')->default(false);
            $table->integer('ordering');
            $table->boolean('is_active');
            $table->boolean('trashed')->default(false);
            $table->tinyInteger('trashed_by');
            $table->timestamp('trashed_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
