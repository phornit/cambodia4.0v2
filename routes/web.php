<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Auth::routes();
Route::get('/admin', 'AdminController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/send/email', 'HomeController@mail');

Route::resource('content', 'Frontend\ContentController');
Route::resource('cambodiaCenter', 'Frontend\CambodiaCenterController');
Route::resource('subjects', 'Frontend\SubjectController');
Route::resource('lesson', 'Frontend\LessonController');

Route::get('member', 'Frontend\MemberConteroller@member');



// Bondol Test
Route::get('/bondol_test','Frontend\BondolTestController@digital_interaction');
Route::get('/contact','Frontend\BondolTestController@contact');
Route::get('/news','Frontend\BondolTestController@news');

