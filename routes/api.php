<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(['users' => 'Auth\UserController']);
Route::apiResources(['role' => 'Auth\RoleController']);
Route::apiResources(['mainMenu' => 'Backend\MainMenuController']);

Route::apiResources(['youngMathScience' => 'Backend\YoungMathScienceController']);
Route::put('youngMathScienceApprove/{id}', 'Backend\YoungMathScienceController@approve');

Route::apiResources(['startupClub' => 'Backend\StartupClubController']);
Route::put('startupClubApprove/{id}', 'Backend\StartupClubController@approve');

Route::apiResources(['digitalTechnologyScience' => 'Backend\DigitalTechnologyScienceController']);
Route::put('digitalTechnologyScienceApprove/{id}', 'Backend\DigitalTechnologyScienceController@approve');

Route::get('cambodiaCenterLegalEntityList', 'Backend\CambodiaCenterController@cambodiaCenterLegalEntityList');
Route::get('cambodiaCenterPhysicalPersonList', 'Backend\CambodiaCenterController@cambodiaCenterPhysicalPersonList');
Route::put('cambodiaCenterApprove/{id}', 'Backend\CambodiaCenterController@approve');

Route::apiResources(['contentCategory' => 'Backend\ContentCategoryController']);
Route::get('getAllCategories', 'Backend\ContentCategoryController@getAllCategories');
Route::get('findContentCategory', 'Backend\ContentCategoryController@search');
Route::get('parents', 'Backend\ContentCategoryController@parents');

Route::apiResources(['content' => 'Backend\ContentController']);
Route::get('findContent', 'Backend\ContentController@search');

Route::get('findUser', 'Auth\UserController@search');

Route::put('trash/{id}', 'Backend\ContentController@trash');

//CoreBackEnd
Route::post('fileUpload', 'Backend\FileUploadController@upload');
Route::post('deleteImg/{img}', 'Backend\FileUploadController@removeImage');
Route::post('upload', 'Backend\FileUploadController@fileUpload');
Route::post('resizeImagePost', 'Backend\FileUploadController@resizeImagePost');


