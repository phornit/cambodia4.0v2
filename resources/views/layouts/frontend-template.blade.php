<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- reponsive meta-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ទំព័រដើម | កម្ពុជា៤.០</title>
    <meta name="description" content="Cambodia4point0,Cambodia4point0 center,Cambodia4.0,Cambodia 4.0,Cam4.0,Cam 4.0,Cambodia digital center,មជ្ឈមណ្ឌលកម្ពុជា៤.០,កម្ពុជា៤.០,កម្ពុជា ៤.០,digital cambodia" />
    <meta name="keywords" content="Cambodia4point0,Cambodia4point0 center,Cambodia4.0,Cambodia 4.0,Cam4.0,Cam 4.0,Cambodia digital center,មជ្ឈមណ្ឌលកម្ពុជា៤.០,កម្ពុជា៤.០,កម្ពុជា ៤.០,digital cambodia" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap-->
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- animate css-->
    <link rel="stylesheet" href="{{ asset('frontend/css/animate.min.css') }}">
    <!-- owl-carousel-->
    <link rel="stylesheet" href="{{ asset('frontend/vendors/owlcarousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/vendors/revolution/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/vendors/revolution/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/vendors/revolution/css/navigation.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/vendors/jquery-ui-1.11.4/jquery-ui.min.css') }}">
    <!-- Main Css-->
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
    <link rel="shortcut icon" href="http://cambodia4point0.org/images/logo_small.png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    -->




    <style lang="scss">
        a {
            color: rgb(0, 0, 0);
        }
        @font-face {
            font-family: "Odor Mean Chey";
            src: url('/fonts/khmer-font/KhmerOSContent-Bold.ttf');
            src: url('/fonts/khmer-font/KhmerOSContent-Regular.ttf');
            src: url('/fonts/khmer-font/OdorMeanChey.ttf');
            font-weight: 400;
            font-style: normal;
        }
    </style>

  </head>
  <body>
  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0"></script>

    <header id="header">
      <!-- Header Gray Band-->
      <section class="wel-t-band">
        <div class="container">
          <div class="row wel-band-bg">
            <div class="col-md-6 pull-left col col-sm-6 wel-left text-left">
              <p>សូមស្វាគមន៍ដែលបានចូលមកគេហទំព័រ  <span> មជ្ឈមណ្ឌលកម្ពុជា ៤.០</span></p>
            </div>
            <div class="col-md-6 pull-right text-right col-sm-3">
              <p><a href="/login">ចូលប្រព័ន្ធ</a> | ចុះឈ្មោះ</p>
            </div>
          </div>
        </div>
      </section>
      <!-- Header Gray Band-->
      <!-- Logo Section-->
      <div class="container header-top">
        <div class="row pad-logo logo-wrapper">
          <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 t-logo" style="float: left;">
              <a href="index-2.html"><img src="{{ asset('frontend/images/header/logo-2.png') }}" alt="logo" class="img-responsive"></a>
          </div>

          <div class="col-lg-8 col-md-6 col-sm-4 col-xs-4 text-right top-center">
            <div class="top-two-right">
              <div class="req-button text-right"><a href="{{url('member')}}" class="submit">ពាក្យសុំចូលជាសមាជិក</a></div>
              <div class="top-panel">
                <div class="touch_top touch_top_pad">
                  <ul class="nav">
                    <li class="item item-phone">
                      <div class="media">
                        <div class="blue-color media-left"><a href="#"><i class="icon icon-Phone2"></i></a></div>
                        <div class="media-body">
                          <p>(+855)12 628 665<br><span><a href="mailto:info@industrial.com">info@cambodia4point0.org</a></span></p>
                        </div>
                      </div>
                    </li>
                    <li class="item item-ad">
                      <div class="media">
                        <div class="blue-color media-left"><a href="#"><i class="icon icon-Pointer"></i></a></div>
                        <div class="media-body">
                          <p>13005 Greenville Avenue<br><span>California, TX 70240</span></p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

            <!-- for mobile --->
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-4 text-right req-button-2">
                <div class="req-button"><a href="{{url('member')}}" class="submit">ពាក្យសុំចូលជាសមាជិក</a></div>
            </div>

            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 t-logo" style="float: right;">
                <a href="index-2.html"><img src="{{ asset('frontend/images/header/logo-3.png') }}" alt="logo" class="img-responsive"></a>
            </div>

        </div>
      </div>
      <!-- Main Navigation-->
      <div class="main_menu menu_fixed nav-home-three">
        <div class="container">
          <div class="row">

            <div class="col-md-3 col-sm-4 col-xs-4">
              <div class="logo"><img src="{{ asset('frontend/images/header/logo-2.png') }}" alt="" style="height: 60px;"></div>
            </div>

          <div class="col-md-3 col-sm-4 col-xs-4">
              <div class="logo"><img src="{{ asset('frontend/images/header/logo-3.png') }}" alt="" style="height: 60px;"></div>
          </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="nav-menu pull-left text-left">
                <div class="nav-t-holder pull-left text-left">
                  <div class="nav-t-header">
                    <button><i class="fa fa-bars"></i></button>
                  </div>
                  <div class="nav-t-footer">



                    <ul class="nav">
                      <li><a href="/">ទំព័រដើម</a>
                      </li>
                      <li class="has-t-submenu"><a href="about.html">អំពីយើង</a>
                        <ul class="submenu">
                          <li><a href="#">មជ្ឈមណ្ឌលកម្ពុជា ៤.០</a></li>
                          <li><a href="#">តារាងរចនាសម្ព័ន្ធ</a></li>
                          <li><a href="#">សៀវភៅ Branding</a></li>
                        </ul>
                      </li>
                        <li><a href="#">កម្មវិធីអក្ខរកម្មឌីជីថល</a></li>
                        <li><a href="#">ពីនេះពីនោះ</a></li>
                        <li><a href="{{url('/subjects')}}">E-Learning</a></li>
                        <li><a href="contact.html">ទំនាក់ទំនង</a></li>
                    </ul>
{{--                    <div class="mobile-link"><a href="request-qoute.html" class="submit">request a quote</a>--}}
{{--                      <div class="widget-t widget-t-search">--}}
{{--                        <div class="widget-t-inner">--}}
{{--                          <form action="#" method="get" class="search-form">--}}
{{--                            <div class="input-group">--}}
{{--                              <input type="search" placeholder="Search" class="form-control"><span class="input-group-addon">--}}
{{--                                <button type="submit"><i class="icon icon-Search"></i></button></span>--}}
{{--                            </div>--}}
{{--                          </form>--}}
{{--                        </div>--}}
{{--                      </div>--}}
{{--                    </div>--}}
                  </div>
                </div>
              </div>
              <div class="nav-search pull-right text-right">
                <div class="widget-t widget-t-search">
                  <div class="widget-t-inner">
                    <form action="#" method="get" class="search-form">

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Main Navigation-->
    </header>
    @yield('content')

    <footer class="sec-padding footer-bg footer-bg3">
      <div class="container clearfix">
        <div class="row">
          <div class="widget about-us-widget col-md-3 col-sm-6"><a href="#"><img src="http://127.0.0.1:8000/frontend/images/header/logo-2.png" alt=""></a>
            <p>ត្រូវបង្កើតឡើងក្នុងគោលបំណង ដើម្បីចូលរួមជាមួយរាជរដ្ឋាភិបាល និងភាគីពាក់ព័ន្ធ ក្នុងការកសាងសេដ្ឋកិច្ចឌីជីថលនៅកម្ពុជា ពិសេសដាក់ចេញយុទ្ធនាការ “កម្ពុជា ៤.០” ដោយអនុលោមតាមគោលការណ៍គ្រឹះចំនួន៥ ដូចខាងក្រោម...</p><a href="about.html">អានបន្តរ... <i class="fa fa-angle-double-right"></i></a>
            <ul class="nav">
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
              <li><a href="#"><i class="fa fa-skype"></i></a></li>
              <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
            </ul>
          </div>
          <div class="widget widget-links col-md-3 col-sm-6">
            <h4 class="widget_title">ពាក្យស្នើសុំចូលជាសមាជិក</h4>
            <div class="widget-contact-list row m0">
              <ul>
                <li><a href="meterial.html">- មជ្ឈមណ្ឌលកម្ពុជា ៤.០</a></li>
                <li><a href="agricultural.html">- ក្លឹបអ្នកជំនាញបច្ចេកវិទ្យាឌីជីថល និងអ្នកវិទ្យាសាស្រ្ត</a></li>
                <li><a href="mechanical.html">- ក្លឹបធុរកិច្ចថ្មីកម្ពុជា</a></li>
                <li><a href="chemical.html">- ក្លឹបអ្នកគណិតវិទ្យា និងអ្នកវិទ្យាសាស្រ្តវ័យក្មេង (ក.គ.វ)</a></li>
              </ul>
            </div>
          </div>
          <div class="widget widget-links col-md-3 col-sm-6">
            <h4 class="widget_title">Facebook Page</h4>
            <div class="widget-contact-list row m0">
                <div class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/%E1%9E%80%E1%9E%98%E1%9F%92%E1%9E%96%E1%9E%BB%E1%9E%87%E1%9E%B6-%E1%9F%A4%E1%9F%A0-Cambodia-40-2260430647540976" data-tabs="timeline" data-width="241" data-height="250" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=238252776219787&amp;container_width=255&amp;height=370&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2F%25E1%259E%2580%25E1%259E%2598%25E1%259F%2592%25E1%259E%2596%25E1%259E%25BB%25E1%259E%2587%25E1%259E%25B6-%25E1%259F%25A4%25E1%259F%25A0-Cambodia-40-2260430647540976&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline&amp;width=251"><span style="vertical-align: bottom; width: 251px; height: 370px;"><iframe name="f3bc57baa0a2fbc" width="251px" height="370px" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.0/plugins/page.php?adapt_container_width=true&amp;app_id=238252776219787&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D45%23cb%3Df33b25502c0308%26domain%3Dcambodia4point0.org%26origin%3Dhttp%253A%252F%252Fcambodia4point0.org%252Ff2dfb5e444c9fac%26relation%3Dparent.parent&amp;container_width=255&amp;height=370&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2F%25E1%259E%2580%25E1%259E%2598%25E1%259F%2592%25E1%259E%2596%25E1%259E%25BB%25E1%259E%2587%25E1%259E%25B6-%25E1%259F%25A4%25E1%259F%25A0-Cambodia-40-2260430647540976&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline&amp;width=251" style="border: none; visibility: visible; width: 251px; height: 370px;" class=""></iframe></span></div>
            </div>
          </div>
          <div class="widget widget-contact col-md-3 col-sm-6">
            <h4 class="widget_title">ទំនាក់ទំនង</h4>
            <div class="widget-contact-list row m0">
              <ul>
                <li><i class="fa fa-map-marker"></i>
                  <div class="fleft location_address">Lorance 542B, Tailstoi Town 5248 MT, Wordwide Country</div>
                </li>
                <li><i class="fa fa-phone"></i>
                  <div class="fleft contact_no"><a href="#">(+855) 12 628 665</a></div>
                </li>
                <li>
                    <i class="fa fa-envelope-o"></i>
                    <div class="fleft contact_mail"><a href="mailto:info@cambodia4point0.org">info@cambodia4point0.org</a></div>
                </li>
                <li>
                    <i class="icon icon-Timer"></i>
                    <div class="fleft service_time">Mon - Sat : 9am to 6pm</div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <section class="footer-t-bottom footer-bottom footer-bottom3">
      <div class="container clearfix">
        <div class="pull-left fo-txt">
          <p>© ២០២០ រក្សាសិទ្ធិដោយមជ្ឈមណ្ឌលកម្ពុជា ៤.០</p>
        </div>
        <div class="pull-right fo-txt">
          <p></p>
        </div>
      </div>
    </section>
    <script src="{{ asset('frontend/js/jquery-1.12.2.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <!-- Revolution Slider Tools-->
    <script src="{{ asset('frontend/vendors/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
    <!-- Revolution Slider-->
    <script type="text/javascript" src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.form.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('frontend/js/contact.js') }}"></script>
    <!-- owl carousel-->
    <script src="{{ asset('frontend/vendors/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontend/vendors/jquery-ui-1.11.4/jquery-ui.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTI1BZJKFCeJ8g9O01CMZ4QvVCfdPYkrE"></script>
    <script src="{{ asset('frontend/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('frontend/js/imagelightbox.min.js') }}"></script>
    <script src="{{ asset('frontend/js/theme.js') }}"></script>
  </body>

</html>
