
@extends('layouts.frontend-template')
@section('content')

<style>
  label,p{

    font-weight: 400;
    font-family: 'KhmerOSContent',serif;
    color: #171819;
    font-size: 16px !important;
    line-height: 26px;

  }
</style>


  <div class="container">
    <div class="row">
      <div class="col-md-6">
          <h3>ទំនាក់ទំនង</h3>
          <div class="form-group"> 
            <label>ឈ្មោះ</label>
            <input type="text" name="name" placeholder="ឈ្មោះ" class="form-control">
          </div>

          <div class="form-group">
            <label>អ៊ីមែល</label>
            <input type="text" name="email" placeholder="អ៊ីមែល" class="form-control">
          </div>

          <div class="form-group">
            <label>ប្រធានបទ</label>
            <input type="text" name="subject" placeholder="ប្រធានបទ" class="form-control">
          </div>

          <div class="form-group">
            <label>ប្រធានបទ</label>
            <textarea rows="5" cols="20" name="message" class="form-control"></textarea>
          </div>

          <button class="btn btn-primary">ផ្ញើសារ</button>

      </div>

      <div class="col-md-6">
        <h3>ការចុះ​ឈ្មោះ</h3>
        <p style="text-align: justify;">មជ្ឈមណ្ឌលកម្ពុជា ៤.០ ត្រូវបានបង្កើតឡើងក្នុងបុព្វហេតុចូលរួមជាមួយរាជរដ្ឋាភិបាល និងភាគីពាក់ព័ន្ធក្នុងការកសាងសេដ្ឋកិច្ចឌីជីថលនៅកម្ពុជាតាមរយៈការបណ្តុះបណ្តាល និងតម្រង់ទិស ផ្នែកជំនាញឌីជីថល និងធុរកិច្ចថ្មីផ្អែកលើថ្នាលបច្ចេកវិទ្យា ការផ្សារភ្ជាប់បណ្តាញការងារ ការសិក្សា ស្រាវជ្រាវ អភិវឌ្ឍន៍ និង​ពិគ្រោះ​យោបល់​លើ​សហគ្រិន​ភាព​ផ្នែកជំនាញ​ឌីជីថល និងបណ្តុះស្មារតីស្រលាញ់ការសិក្សា និងស្រាវជ្រាវផ្នែកបច្ចេកវិទ្យា និងវិទ្យាសាស្រ្តទេពកោសល្យ គំនិតច្នៃប្រឌិត និងការបង្កើតថ្មី។ មជ្ឈមណ្ឌលកម្ពុជា ៤.០ មានបេសកកម្ម​បណ្តុះ​បណ្តាលអ្នកជំនាញឌីជីថល និងផ្តល់ការគាំទ្រដល់ការបង្កើតសហគ្រិនឌីជីថលឲ្យលូតលាស់ និងមានភាពជោគជ័យ។</p>

        <p>លេខទូរស័ព្ទ : 012 55566</p>
        <p>អ៊ីមែល : cambodia4.0@gmail.com</p>
        <p>វេបសាយ : cambodia4.0.com.org</p>
        <br/>
        <img src="{{url('images/contact_img.jpg')}}" class="img-fluid" width="500" height="300">
        <br /><br /><br />
      </div>

      

      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7818.368720204843!2d104.912516007787!3d11.538629068058826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310951cc6c2c58db%3A0x8e47eb14c43c6b15!2sCambodia%204.0!5e0!3m2!1sen!2skh!4v1575364227413!5m2!1sen!2skh" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>


    </div>
  </div>




@endsection