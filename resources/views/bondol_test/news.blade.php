
@extends('layouts.frontend-template')
@section('content')

	<!--1. ប្រភពព័ត៌មានជាតិផ្សេងៗ -->
 	<section class="layout-boxed diff-offer-wrapper">
        <div class="container">
         	<h1>ប្រភពព័ត៌មានជាតិផ្សេងៗ</h1>
            <div class="row">
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/1" class="post-image view image_hover">
                            <img src="http://digitalyouthcambodia.org/images/contents/2020/fn-2020-01-08-15-16-14-0.jpg" alt="" class="img-responsive zoom_img_effect">
                        </a><a href="/content/1">
                            <h4>បណ្ណាល័យអេឡិចត្រូនិកមួយ បណ្ដុំទៅដោយព័ត៌មាន និងមេរៀនល្អៗ សម្រាប់អ្នកអានដាក់ឲ្យដំណើរការ</h4></a>
                        <p>(ភ្នំពេញ)៖ Mobile App «គំនិត» ឬក៏អាចនិយាយបានថា ជាបណ្ណាល័យ Electronic ដ៏ធំមួយ ដែលអ្នកអានអាចស្វែងរកបាននូវព័ត៌មាន និងមេរៀនល្អៗ​...</p>
                        <h6></h6>
                    </div>
                </div>
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/2" class="post-image view image_hover">
                            <img src="http://cambodia4point0.org/images/thumbnails/19650.png" alt="" class="img-responsive zoom_img_effect"></a>
                        <a href="/content/1">
                            <h4>ប្រសាសន៍ដ៏ខ្ពង់ខ្ពស់របស់សម្តេចតេជោ ហ៊ុន សែន នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា អំពីអក្ខរកម្មឌីជីថល</h4></a>
                        <p>ប្រសាសន៍ដ៏ខ្ពង់ខ្ពស់របស់សម្តេចតេជោ ហ៊ុន សែន នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា អំពីអក្ខរកម្មឌីជីថល នាឱកាសជំនួបជាមួយអ្នកសារព័ត៌មាន កាលពីថ្ងៃទី ១៤ មករា ឆ្នាំ២០២០ ...</p>
                        <h6></h6>
                    </div>
                </div>
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/3" class="post-image view image_hover">
                            <img src="http://digitalyouthcambodia.org/images/contents/content1/New%20Folder1/img-0.66708900%201577511507%20(1).jpg" alt="" class="img-responsive zoom_img_effect"></a>
                        <a href="/content/1">
                            <h4>មិនធម្មតា! អាយុ ១០ ឆ្នាំក្លាយជា CEO បច្ចេកវិទ្យាក្មេងបំផុត ឡើងឆាកនិយាយមុខមនុស្ស ២០០០០ នាក់</h4></a>
                        <p>ជារឿងគួរភ្ញាក់ផ្អើលដែលក្មេងស្រីអាយុ ១០ ឆ្នាំម្នាក់ មានសមត្ថភាពអាចបកស្រាយពីបច្ចេកវិទ្យាប៊ុកឆេន និងបង្រៀនក្មេងៗដទៃទៀត ជាពិសេសគាត់បានក្លាយជា CEO ...</p>
                        <h6></h6>
                    </div>
                </div>
                               
            </div>


        </div>
    </section>

    <!-- 2. ប្រភពព័ត៌មានអន្តរជាតិផ្សេងៗ -->
    <section class="layout-boxed diff-offer-wrapper">
        <div class="container">
         	<h1>ប្រភពព័ត៌មានអន្តរជាតិផ្សេងៗ</h1>
            <div class="row">
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/1" class="post-image view image_hover">
                            <img src="http://digitalyouthcambodia.org/images/contents/2020/fn-2020-01-08-15-16-14-0.jpg" alt="" class="img-responsive zoom_img_effect">
                        </a><a href="/content/1">
                            <h4>បណ្ណាល័យអេឡិចត្រូនិកមួយ បណ្ដុំទៅដោយព័ត៌មាន និងមេរៀនល្អៗ សម្រាប់អ្នកអានដាក់ឲ្យដំណើរការ</h4></a>
                        <p>(ភ្នំពេញ)៖ Mobile App «គំនិត» ឬក៏អាចនិយាយបានថា ជាបណ្ណាល័យ Electronic ដ៏ធំមួយ ដែលអ្នកអានអាចស្វែងរកបាននូវព័ត៌មាន និងមេរៀនល្អៗ​...</p>
                        <h6></h6>
                    </div>
                </div>
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/1" class="post-image view image_hover">
                            <img src="http://cambodia4point0.org/images/thumbnails/19650.png" alt="" class="img-responsive zoom_img_effect"></a>
                        <a href="/content/1">
                            <h4>ប្រសាសន៍ដ៏ខ្ពង់ខ្ពស់របស់សម្តេចតេជោ ហ៊ុន សែន នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា អំពីអក្ខរកម្មឌីជីថល</h4></a>
                        <p>ប្រសាសន៍ដ៏ខ្ពង់ខ្ពស់របស់សម្តេចតេជោ ហ៊ុន សែន នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា អំពីអក្ខរកម្មឌីជីថល នាឱកាសជំនួបជាមួយអ្នកសារព័ត៌មាន កាលពីថ្ងៃទី ១៤ មករា ឆ្នាំ២០២០ ...</p>
                        <h6></h6>
                    </div>
                </div>
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/1" class="post-image view image_hover">
                            <img src="http://digitalyouthcambodia.org/images/contents/content1/New%20Folder1/img-0.66708900%201577511507%20(1).jpg" alt="" class="img-responsive zoom_img_effect"></a>
                        <a href="/content/1">
                            <h4>មិនធម្មតា! អាយុ ១០ ឆ្នាំក្លាយជា CEO បច្ចេកវិទ្យាក្មេងបំផុត ឡើងឆាកនិយាយមុខមនុស្ស ២០០០០ នាក់</h4></a>
                        <p>ជារឿងគួរភ្ញាក់ផ្អើលដែលក្មេងស្រីអាយុ ១០ ឆ្នាំម្នាក់ មានសមត្ថភាពអាចបកស្រាយពីបច្ចេកវិទ្យាប៊ុកឆេន និងបង្រៀនក្មេងៗដទៃទៀត ជាពិសេសគាត់បានក្លាយជា CEO ...</p>
                        <h6></h6>
                    </div>
                </div>
                               
            </div>

            
        </div>
    </section>

    <!--3. ចំណេះដឹងពីនេះពីនោះ -->
    <section class="layout-boxed diff-offer-wrapper">
        <div class="container">
         	<h1>ចំណេះដឹងពីនេះពីនោះ</h1>
            <div class="row">
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/1" class="post-image view image_hover">
                            <img src="http://digitalyouthcambodia.org/images/contents/2020/fn-2020-01-08-15-16-14-0.jpg" alt="" class="img-responsive zoom_img_effect">
                        </a><a href="/content/1">
                            <h4>បណ្ណាល័យអេឡិចត្រូនិកមួយ បណ្ដុំទៅដោយព័ត៌មាន និងមេរៀនល្អៗ សម្រាប់អ្នកអានដាក់ឲ្យដំណើរការ</h4></a>
                        <p>(ភ្នំពេញ)៖ Mobile App «គំនិត» ឬក៏អាចនិយាយបានថា ជាបណ្ណាល័យ Electronic ដ៏ធំមួយ ដែលអ្នកអានអាចស្វែងរកបាននូវព័ត៌មាន និងមេរៀនល្អៗ​...</p>
                        <h6></h6>
                    </div>
                </div>
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/1" class="post-image view image_hover">
                            <img src="http://cambodia4point0.org/images/thumbnails/19650.png" alt="" class="img-responsive zoom_img_effect"></a>
                        <a href="/content/1">
                            <h4>ប្រសាសន៍ដ៏ខ្ពង់ខ្ពស់របស់សម្តេចតេជោ ហ៊ុន សែន នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា អំពីអក្ខរកម្មឌីជីថល</h4></a>
                        <p>ប្រសាសន៍ដ៏ខ្ពង់ខ្ពស់របស់សម្តេចតេជោ ហ៊ុន សែន នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា អំពីអក្ខរកម្មឌីជីថល នាឱកាសជំនួបជាមួយអ្នកសារព័ត៌មាន កាលពីថ្ងៃទី ១៤ មករា ឆ្នាំ២០២០ ...</p>
                        <h6></h6>
                    </div>
                </div>
                <div class="col-sm-4 service-info">
                    <div class="item">
                        <a href="/content/1" class="post-image view image_hover">
                            <img src="http://digitalyouthcambodia.org/images/contents/content1/New%20Folder1/img-0.66708900%201577511507%20(1).jpg" alt="" class="img-responsive zoom_img_effect"></a>
                        <a href="/content/1">
                            <h4>មិនធម្មតា! អាយុ ១០ ឆ្នាំក្លាយជា CEO បច្ចេកវិទ្យាក្មេងបំផុត ឡើងឆាកនិយាយមុខមនុស្ស ២០០០០ នាក់</h4></a>
                        <p>ជារឿងគួរភ្ញាក់ផ្អើលដែលក្មេងស្រីអាយុ ១០ ឆ្នាំម្នាក់ មានសមត្ថភាពអាចបកស្រាយពីបច្ចេកវិទ្យាប៊ុកឆេន និងបង្រៀនក្មេងៗដទៃទៀត ជាពិសេសគាត់បានក្លាយជា CEO ...</p>
                        <h6></h6>
                    </div>
                </div>
                               
            </div>

            
        </div>
    </section>

@endsection