

@extends('layouts.frontend-template')
@section('content')

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {box-sizing: border-box}

/* Set height of body and the document to 100% */
body, html {
  height: 100%;
  margin: 0;
  font-family: "Odor Mean Chey";
}

/* Style tab links */
.tablink {
  background-color: #555;
  color: white;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  font-size: 17px;
  width: 25%;
}

.tablink:hover {
  background-color: #777;
}

/* Style the tab content (and add height:100% for full page content) */
.tabcontent {
  color: black;
  display: none;
  padding: 100px 20px;
  height: 100%;
}

.container-fluid{

  margin-top: 20px;
}

/*#Home {background-color: red;}
#News {background-color: green;}
#Contact {background-color: blue;}
#About {background-color: orange;}*/
</style>
</head>
<body>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">

        <!-- Tab Menu -->
          <button class="tablink" onclick="openPage('Home', this, '#777')">ការសិក្សាសម្រាប់គ្រូបង្គល</button>
          <button class="tablink" onclick="openPage('News', this, '#777')" id="defaultOpen">ការសិក្សាសម្រប់ប្រជាជនទូទៅ</button>
          <button class="tablink" onclick="openPage('Contact', this, '#777')">ចុះឈ្មោះ</button>
          <button class="tablink" onclick="openPage('About', this, '#777')">ពន៏មានបន្ថែម</button>


          <!-- 1. ការសិក្សាសម្រាប់គ្រូបង្គល -->
          <div id="Home" class="tabcontent">
            <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <h3>ការសិក្សាសម្រាប់គ្រូបង្គល</h3>
                   
                    <table class="table table-hover">

                        <thead>
                          <tr class="bg-info">
                            <th scope="col">#</th>
                            <th scope="col">ឈ្មោះ</th>
                            <th scope="col">វគ្គបណ្តុះបណ្តាល</th>
                            <th scope="col">គោលបំណង</th>
                            <th scope="col">សកម្មភាព</th>
                          </tr>
                        </thead>

                        <tbody>

                          
                          <tr>
                            <td scope="row">12</td>
                            <td>យ៉ែមបណ្តូល</td>
                            <td>Digital Maketing </td>
                            <td>ការសិក្សាសម្រាប់គ្រូបង្គល</td>
                            <td>
                              <a href="#" class="btn btn-success btn-sm">Edit</a>
                            </td>
                          </tr>

                          <tr>
                            <td scope="row">10</td>
                            <td>Vannaroth</td>
                            <td>Accounting  </td>
                            <td>ការសិក្សាសម្រប់ប្រជាជនទូទៅ</td>
                            <td>
                              <a href="#" class="btn btn-success btn-sm">Edit</a>
                            </td>
                          </tr>

                          <tr>
                            <td scope="row">8</td>
                            <td>Modany</td>
                            <td>IT Software</td>
                            <td>ការសិក្សាសម្រាប់គ្រូបង្គល</td>
                            <td>
                              <a href="#" class="btn btn-success btn-sm">Edit</a>
                            </td>
                          </tr>
                          
                          
                        </tbody>

                    </table>
                  </div>
                </div>
              </div>
          </div>

          <!-- 2. ការសិក្សាសម្រាប់ប្រជាជនទូទៅ -->
          <div id="News" class="tabcontent">
            <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <h3>ការសិក្សាសម្រប់ប្រជាជនទូទៅ</h3>
                   
                    <table class="table table-hover">

                        <thead>
                          <tr class="bg-info">
                            <th scope="col">#</th>
                            <th scope="col">ឈ្មោះ</th>
                            <th scope="col">វគ្គបណ្តុះបណ្តាល</th>
                            <th scope="col">គោលបំណង</th>
                            <th scope="col">សកម្មភាព</th>
                          </tr>
                        </thead>

                        <tbody>

                          
                          <tr>
                            <td scope="row">12</td>
                            <td>យ៉ែមបណ្តូល</td>
                            <td>Digital Maketing </td>
                            <td>ការសិក្សាសម្រាប់គ្រូបង្គល</td>
                            <td>
                              <a href="#" class="btn btn-success btn-sm">Edit</a>
                            </td>
                          </tr>

                          <tr>
                            <td scope="row">10</td>
                            <td>Vannaroth</td>
                            <td>Accounting  </td>
                            <td>ការសិក្សាសម្រប់ប្រជាជនទូទៅ</td>
                            <td>
                              <a href="#" class="btn btn-success btn-sm">Edit</a>
                            </td>
                          </tr>

                          <tr>
                            <td scope="row">8</td>
                            <td>Modany</td>
                            <td>IT Software</td>
                            <td>ការសិក្សាសម្រាប់គ្រូបង្គល</td>
                            <td>
                              <a href="#" class="btn btn-success btn-sm">Edit</a>
                            </td>
                          </tr>
                          
                          
                        </tbody>

                    </table>
                  </div>
                </div>
              </div>
          </div>


          <!-- 3. Register Literacy -->
          <div id="Contact" class="tabcontent">
           
            <!-- Inner Header-->
            <section class="inner-banner2 clearfix">
              <div class="container clearfix">
                <h2>Checkout Page</h2>
              </div>
            </section>
            <section class="breadcumb-wrapper">
              <div class="container clearfix">
                <ul class="breadcumb">
                  <li><a href="index-2.html">Home</a></li>
                  <li><span>Checkout Page</span></li>
                </ul>
              </div>
            </section>


            <!-- News Page-->
            <section id="checkout-content" class="sec-padding">
              <div class="container">

                <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 left-checkout">

                    <div class="section_header color">
                      <h2>ពាក្យសុំចុះឈ្មោះវគ្គបណ្តុះបណ្តាលកម្មវិធីអក្ខរកម្មឌីជីថល</h2>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <label>ឈ្មោះ <span>*</span></label>
                        <input type="text" name="name">
                      </div>
                      <div class="col-lg-6">
                        <label>ឈ្មោះជាអក្សរឡាតាំង <span>*</span></label>
                        <input type="text" name="name_en">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-6">
                        <label>ភេទ <span>*</span></label>
                        <div class="select-box">
                          <select name="sex" class="select-menu">
                            <option value="default">Select An Option</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <label>ថ្ងៃខែកំណើត <span>*</span></label>
                         <input type="text" name="date_of_birth">
                      </div>
                      
                    </div>

                    <div class="row">
                      <div class="col-lg-6">
                        <label>វគ្គបណ្តុះបណ្តាល <span>*</span></label>
                        <div class="select-box">
                          <select name="course" class="select-menu">
                            <option value="">ជ្រើសរើសវគ្គបណ្តុបណ្តាល</option>
                            <option value="digital">Digital Maketing </option>
                            <option value="safe_digital">សុវត្ថិភាពឌីជីថល</option>
                            <option value="digital_cloud">រដ្ឋាបាលឌីជីថល និង Cloud Computing </option>
                          </select>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <label>គោលបំណង <span>*</span></label>
                        <div class="select-box">
                          <select name="purpose" class="select-menu">
                            <option value="">ជ្រើសរើសគោលបំណង</option>
                            <option value="digital">ការសិក្សាសម្រាប់គ្រូបង្គល </option>
                            <option value="safe_digital">ការសិក្សាសម្រប់ប្រជាជនទូទៅ</option>
                          </select>
                        </div>
                      </div>
                      
                    </div>

                   

                    <div class="row">
                      <div class="col-lg-6">
                        <label>ក្រុមហ៊ុន ឬស្ថាប័ន</label>
                        <input type="text" name="company">
                      </div>

                      <div class="col-lg-6">
                        <label>តួនាទី/ជំនាញ </label>
                        <input type="text" name="occupation">
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-lg-6">
                        <label>លេខទូរស័ព្ទ</label>
                        <input type="text" name="phone">
                      </div>

                      <div class="col-lg-6">
                        <label>អុីម៉ែល </label>
                        <input type="text" name="email">
                      </div>

                    </div>
                    
                    <button class="btn btn-primary">ចុះឈ្មោះ</button>
                      


                   
                    
                  </div>
                
                </div>
              </div>
            </section>
          </div>

          <!-- 4. Info Detail -->
          <div id="About" class="tabcontent">
           
            <!-- Inner Header-->
            <section class="inner-banner2 clearfix">
              <div class="container clearfix">
                <h2>More Detail</h2>
              </div>
            </section>
            <section class="breadcumb-wrapper">
              <div class="container clearfix">
                <ul class="breadcumb">
                  <li><a href="index-2.html">Home</a></li>
                  <li><span>More Detail</span></li>
                </ul>
              </div>
            </section>


            <!-- News Page-->
            <section id="checkout-content" class="sec-padding">
              <div class="container">

                <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 left-checkout">

                    <div class="section_header color">
                      <h2>ព័​ត៍​មាន​បន្ថែម</h2>
                    </div>
                    <br /><br /><br />

                    <div class="section_header color">
                      <h2>គោលបំណងនៃសិក្ខាសាលា</h2>
                      <p>មជ្ឈមណ្ឌលកម្ពុជា ៤.០ រៀបចំសិក្ខសាលានេះឡើងក្នុងគោលបំណងពង្រឹងជំនាញឌីជីថលដល់បុគ្គលិកនៃ បណ្តាក្រុមហ៊ុន សហគ្រិន​ សិស្ស-និសិ្សត និង ប្រជាពលរដ្ឋទូទៅ ដែលប្រកបការងារ និងមានចំណាប់អារម្មណ៍លើការផ្សព្វផ្សាយទីផ្សារបែបឌីជីថល ស្របតាមបរិបទនៃសេដ្ឋកិច្ចឌីជីថល និងនិន្នាការនៃបច្ចេកវិទ្យាព័ត៌មានវិទ្យាបច្ចុប្បន្ន។ សិក្ខាសាលានេះនឹងផ្តល់ចំណេះដឹង និងបទពិសោធន៍ពីអ្នកជំនាញពីរបៀបផ្សព្វផ្សាយពាណិជ្ជកម្មតាមអនឡាញ បណ្តាញសង្គមល្បី(Facebook) ​សម្រាប់ជួយពង្រឹងសមត្ថភាពអ្នកចូលរួម បង្កើនប្រសិទ្ធភាពការងារ និងអាជីវកម្មរបស់អង្គភាព កសាង និងពង្រីកម៉ាកសញ្ញាផលិតផល (Brand) អនឡាញដើម្បីចាប់យកអតិថិជនទីផ្សារ​ (potential clients) ឲ្យក្លាយជាអតិថិជនរបស់អ្នក​ នាំមកនូវប្រាក់ចំណូលនិងប្រាក់ចំណេញកើនឡើង។</p>
                    </div>

                    <div class="section_header color">
                      <h2>លទ្ធផលរំពឹងទុកនៃសិក្ខាសាលា</h2>
                      <ul>
                        <li>យល់អំពីការផ្សព្វផ្សាយម៉ាកសញ្ញា (Branding)តាមបណ្តាញ Social media : Facebook</li>
                        <li>យល់ដឹងអំពីយុទ្ធសាស្រ្តរួមនៃ Digital Marketing តាម​​ Social Medi</li>
                        <li>ចេះពីរបៀបបង្កើត និងបញ្ជូនយុទ្ធនាការតាម Digital Marketing</li>
                        <li>ទទួលបានវិញ្ញាបនប័ត្របញ្ជាក់ការចូលរួមវគ្គបណ្តុះបណ្តាលពីមជ្ឈមណ្ឌលកម្ពុជា ៤.០​</li>
                      </ul>
                    </div>

                    <div class="section_header color">
                      <h2>ពេលវេលា និងទីកន្លែង</h2>
                      <ul>
                        <li>ពេលវេលា : ពីម៉ោង 8h ព្រឹក - 13h ព្រឹក ថ្ងៃទី 15-16 ខែសីហា ឆ្នាំ 2019</li>
                        <li>ទីកន្លែង : មជ្ឈមណ្ឌលសហប្រតិបត្តិការ កូរ៉េ - កម្ពុជា (​CKCC)</li>                        
                      </ul>
                    </div>

                    <div class="section_header color">
                      <h2>ទំនាក់ទំនង</h2>
                      <ul>
                        <li>092 56 59 49/016 46 55 67</li>
                        <li>info@cambodia4point0.org</li>                       
                      </ul>
                    </div>




                    <div class="row">
                      <div class="col-lg-6">
                        <label>លេខទូរស័ព្ <span>*</span></label>
                        <input type="text" name="phone">
                      </div>
                      <div class="col-lg-6">
                        <label>អ៊ីមែល  <span>*</span></label>
                        <input type="text" name="email">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12">
                        <label>ខ្លឹមសារ/សំនួរ <span>*</span></label>
                        <input type="text" name="question">
                      </div>
                    </div>

                    <button class="btn btn-primary">ចុះឈ្មោះ</button>

                   
                  </div>
                </div>
              </div>
            </section>
          </div>

      </div>
  </div>
</div>



      <script>
      function openPage(pageName,elmnt,color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(pageName).style.display = "block";
        elmnt.style.backgroundColor = color;
      }

      // Get the element with id="defaultOpen" and click on it
      document.getElementById("defaultOpen").click();
    </script>
       



    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Revolution Slider Tools-->
    <script src="vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Revolution Slider-->
    <script type="text/javascript" src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/contact.js"></script>
    <!-- owl carousel-->
    <script src="vendors/owlcarousel/owl.carousel.min.js"></script>
    <script src="vendors/jquery-ui-1.11.4/jquery-ui.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTI1BZJKFCeJ8g9O01CMZ4QvVCfdPYkrE"></script>
    <script src="js/gmaps.min.js"></script>
    <script src="js/theme.js"></script>
    </body>
    </html> 


@endsection













    


    
  