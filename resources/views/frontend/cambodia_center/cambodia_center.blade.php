@extends('layouts.frontend-template')

@section('content')
    <div>
        <section class="inner-banner2 clearfix">
            <div class="container clearfix">
                <h2>ពាក្យស្នើសុំចូលជាសមាជិក មជ្ឈមណ្ឌលកម្ពុជា ៤.០</h2>
            </div>
        </section>

        <section class="breadcumb-wrapper">
            <div class="container clearfix">
                <ul class="breadcumb">
                    <li><a href="index-2.html">ទំព័រដើម</a></li>
                    <li><span>ពាក្យស្នើសុំចូលជាសមាជិក មជ្ឈមណ្ឌលកម្ពុជា ៤.០</span></li>
                </ul>
            </div>
        </section>

        <section id="checkout-content" class="sec-padding">
            <div class="container">
                <div class="clearfix">
                    <div class="col-lg-12 return-customer">
                        <p>Returning customer? <a href="#">Click here to login</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 left-checkout">
                        <div class="section_header color">
                            <h2>BELLING DETAILS</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label>First Name <span>*</span></label>
                                <input type="text" placeholder="First Name">
                            </div>
                            <div class="col-lg-6">
                                <label>Last Name <span>*</span></label>
                                <input type="text" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Company Name</label>
                                <input type="text" placeholder="Company Name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Address <span>*</span></label>
                                <input type="text" placeholder="Street address">
                                <input type="text" placeholder="Apartment, Suit unit etc (optional)">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Town / City <span>*</span></label>
                                <input type="text" placeholder="Town / City">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label>State / Country <span>*</span></label>
                                <div class="select-box">
                                    <select name="selectMenu" class="select-menu" id="ui-id-1" style="display: none;">
                                        <option value="default">Select An Option</option>
                                        <option value="1">Select An Option</option>
                                        <option value="2">Select An Option</option>
                                        <option value="3">Select An Option</option>
                                        <option value="4">Select An Option</option>
                                    </select><span class="ui-selectmenu-button ui-widget ui-state-default ui-corner-all" tabindex="0" id="ui-id-1-button" role="combobox" aria-expanded="false" aria-autocomplete="list" aria-owns="ui-id-1-menu" aria-haspopup="true" style="width: 133px;"><span class="ui-icon ui-icon-triangle-1-s"></span><span class="ui-selectmenu-text">Select An Option</span></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label>Postcode / Zip <span>*</span></label>
                                <input type="text" placeholder="Postcode / Zip">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Email Address <span>*</span></label>
                                <input type="text" placeholder="Email Address">
                            </div>
                            <div class="col-lg-6">
                                <label>Phone <span>*</span></label>
                                <input type="text" placeholder="Phone">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 create-account">
                                <input type="checkbox"><span><b>Create an account ?</b></span>
                            </div>
                        </div>
                        <div class="section-title2 ship-to-different">
                            <input type="checkbox">
                            <div class="section_header color m0 p0">
                                <h2>SHIP TO DIFFERENT ADDRESS</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Order Notes</label>
                                <textarea placeholder="Note about your order. e.g. special note for delivery"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0 right-checkout">
                        <div class="row">
                            <div class="section_header color">
                                <h2>YOUR ORDER</h2>
                            </div>
                            <div class="col-lg-12 order-box">
                                <ul>
                                    <li>PRODUCT <span>TOTAL</span></li>
                                    <li>Electric Hummber X 1 <span>$65.00</span></li>
                                    <li>SUBTOTAL <span class="bold">$65.00</span></li>
                                    <li>Shipping and Handling <span>Free Shipping</span></li>
                                    <li class="total">TOTAL <span class="bold">$65</span></li>
                                    <li>
                                        <input type="radio"> Direct Bank Payment
                                        <div class="note">
                                            <div class="i fa fa-caret-up"></div>                                        Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.
                                        </div>
                                    </li>
                                    <li>
                                        <input type="radio"> Check Payment
                                    </li>
                                    <li>
                                        <input type="radio"> PayPal <img src="images/paypal.png" alt=""><a href="#"><span>What is PayPal?</span></a>
                                    </li>
                                    <li class="place-order-button clearfix"><a href="#" class="thm-btn"> place order </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
