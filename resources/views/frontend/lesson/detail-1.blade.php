@extends('layouts.frontend-template')

@section('content')
    <div>
        <section class="inner-banner2 clearfix">
            <div class="container clearfix">
                <h2>ការប្រើប្រាស់កំព្យូទ័រឬឧបករណ៍របស់អ្នក</h2>
            </div>
        </section>

        <section class="breadcumb-wrapper">
            <div class="container clearfix">
                <ul class="breadcumb">
                    <li><a href="index-2.html">ទំព័រដើម</a></li>
                    <li><a href="#">E-Learning</a></li>
                    <li><span>ការប្រើប្រាស់កំព្យូទ័រឬឧបករណ៍របស់អ្នក</span></li>
                </ul>
            </div>
        </section>

        <section class="core-projects sectpad">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-md-8 col-sm-12 pull-left">
                        <!-- News-->
                        <div class="single-post-wrapper news news-details">
                            <!-- News 1-->
                            <article class="single-blog-post img-cap-effect">
                                <h3>ការប្រើប្រាស់កំព្យូទ័រឬឧបករណ៍របស់អ្នក</h3>
                                <div class="img-box">
                                    <a href="blog-details.html">
                                        <img src="https://img1.exportersindia.com/product_images/bc-full/2019/7/4597960/desktop-computer-1561967368-4978714.jpeg" alt="" class="img-responsive">
                                    </a>
                                </div>
                                <br/>
                                <p>
                                    រៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេតរៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេតរៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេត។ នៅក្នុងវគ្គសិក្សានេះអ្នកនឹងរៀនអំពីមុខងារមូលដ្ឋានរបស់កុំព្យូទ័រនិងរបៀបប្រើវាដោយសុវត្ថិភាព។ អ្នកនឹងរៀនពីរបៀបបើកនិងបិទកុំព្យូទ័រនិងរបៀបផ្លាស់ប្តូរការកំណត់ដើម្បីឱ្យវាសមនឹងតម្រូវការរបស់អ្នក។ វគ្គសិក្សានេះក៏នឹងបង្រៀនអ្នកអំពីផ្នែកផ្សេងៗនៃកុំព្យូទ័រផងដែរ។ ការប្រើប្រាស់កុំព្យួទ័រអាចធ្វើអោយជីវិតរបស់អ្នកមានភាពងាយស្រួលនិងបើកឱកាសថ្មីៗជាច្រើនសម្រាប់អ្នក។ ការដឹងពីរបៀបតម្រៀបឯកសារនិងថតឯកសាររបស់អ្នកនឹងជួយអ្នកឱ្យប្រើកុំព្យូទ័ររបស់អ្នកកាន់តែមានប្រសិទ្ធភាព។ អ្នកត្រូវប្រាកដថាអ្នកអាចរកឃើញឯកសាររបស់អ្នកមិនថាវាជារូបថតឯកសារឬអ្វីផ្សេងទៀតទេ។
                                </p>

                                <div class="accessibility-state" style="border-top: 1px solid #E6E6E6; padding-top: 20px;">
                                    <a href="/lesson/1" class="btn btn3 btn-sm btn-pre"> <i class="fa fa-arrow-left" aria-hidden="true"></i> ថយក្រោយ</a>
                                    <a href="/lesson/3" class="btn btn-sm btn-next">បន្តាប់ <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </div>

                            </article>
                            <!-- Comments-->
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right news">
                        <div>

                            <div class="single-sidebar-widget">
                                <div class="categories">
                                    <ul>
                                        <li><a href="#">Chemistry of Petrolium Filteration</a></li>
                                        <li><a href="#">Alluminium Strength Depends</a></li>
                                        <li><a href="#">Solar Panel Working Process</a></li>
                                        <li><a href="#">Advanced Materials Innovation</a></li>
                                        <li><a href="#">Metal Manufacturing Process</a></li>
                                        <li><a href="#">Construction and Architecture</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
