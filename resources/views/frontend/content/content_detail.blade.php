@extends('layouts.frontend-template')

@section('content')
    <div>
        <section class="inner-banner2 clearfix">
            <div class="container clearfix">
                <h2>{{ $content->title }}</h2>
            </div>
        </section>

        <section class="breadcumb-wrapper">
            <div class="container clearfix">
                <ul class="breadcumb">
                    <li><a href="/">ទំព័រដើម</a></li>
                    <li><a href="/content">ពីនេះពីនោះ</a></li>
                    <li><span>{{ $content->title }}</span></li>
                </ul>
            </div>
        </section>



        <section class="core-projects sectpad">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-md-8 col-sm-12 pull-left">
                        <!-- News-->
                        <div class="single-post-wrapper news news-details">
                            <!-- News 1-->
                            <article class="single-blog-post img-cap-effect">
                                <div class="img-box">
                                        <img src="{{asset('images/thumbnails')}}/{{$content->thumb_image}}" alt="" class="img-responsive">
                                </div>
                                <div class="meta-info">
                                    <div class="content-box">
                                        <ul class="post-links">
                                            <li><a href="#"><i class="fa fa-user"></i> By:  {!! \App\Libraries\Backend\CoreModel::getUserName($content->created_by) !!}</a></li>
                                            <li><a href="#"><i class="fa fa-tag"></i> Delivery across country</a></li>
                                            <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i> {{ $content->created_at }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h3>{{ $content->title }}</h3>
                                <div col-lg-12 col-md-12 col-sm-12 col-xs-12>
                                    {!! $content->description !!}
                                </div>

                                <div class="fb-comments" data-href="http://newversion.cambodia4point0.org/content/1" data-width="100%" data-numposts="5"></div>

                            </article>
                            <!-- Comments-->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 pull-right content-details">
                        <div>

                            <div class="single-sidebar-widget">
                                <div class="section_header2">
                                    <h2>ពីនេះពីនោះ</h2>
                                </div>
                                <div class="popular-post">
                                    <ul>
                                        <li class="img-cap-effect">
                                            <div class="img-box">
                                                <a href="/content/1">
                                                    <img src="http://cambodia4point0.org/images/thumbnails/19650.png" alt="Awesome Image" style="width: 100%;">
                                                </a></div>
                                            <div class="content">
                                                <a href="/content/1">
                                                    <h4>ប្រសាសន៍ដ៏ខ្ពង់ខ្ពស់របស់សម្តេចតេជោ ហ៊ុន សែន នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា អំពីអក្ខរកម្មឌីជីថល</h4>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="img-cap-effect">
                                            <div class="img-box"><a href="/content/1"><img src="http://cambodia4point0.org/images/thumbnails/33465.jpg" alt="Awesome Image" style="width: 100%;"></a></div>
                                            <div class="content"><a href="/content/1">
                                                    <h4>បណ្ណាល័យអេឡិចត្រូនិកមួយ បណ្ដុំទៅដោយព័ត៌មាន និងមេរៀនល្អៗ សម្រាប់អ្នកអានដាក់ឲ្យដំណើរការ</h4></a>
                                            </div>
                                        </li>
                                        <li class="img-cap-effect">
                                            <div class="img-box"><a href="/content/1"><img src="http://cambodia4point0.org/images/thumbnails/48844.jpg" alt="Awesome Image" style="width: 100%;"></a></div>
                                            <div class="content"><a href="/content/1">
                                                    <h4>មិនធម្មតា! អាយុ ១០ ឆ្នាំក្លាយជា CEO បច្ចេកវិទ្យាក្មេងបំផុត ឡើងឆាកនិយាយមុខមនុស្ស ២០០០០ នាក់</h4></a>
                                            </div>
                                        </li>
                                        <li class="img-cap-effect">
                                            <div class="img-box"><a href="/content/1"><img src="http://cambodia4point0.org/images/thumbnails/19650.png" alt="Awesome Image" style="width: 100%;"></a></div>
                                            <div class="content">
                                                <a href="/content/1">
                                                    <h4>ប្រសាសន៍ដ៏ខ្ពង់ខ្ពស់របស់សម្តេចតេជោ ហ៊ុន សែន នាយករដ្ឋមន្ត្រីនៃព្រះរាជាណាចក្រកម្ពុជា អំពីអក្ខរកម្មឌីជីថល</h4>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="img-cap-effect">
                                            <div class="img-box">
                                                <a href="/content/1">
                                                    <img src="http://cambodia4point0.org/images/thumbnails/33465.jpg" alt="Awesome Image" style="width: 100%;">
                                                </a>
                                            </div>
                                            <div class="content">
                                                <a href="/content/1">
                                                    <h4>បណ្ណាល័យអេឡិចត្រូនិកមួយ បណ្ដុំទៅដោយព័ត៌មាន និងមេរៀនល្អៗ សម្រាប់អ្នកអានដាក់ឲ្យដំណើរការ</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="single-sidebar-widget col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="sec-title">
                                    <h2><span>Tag clouds</span></h2>
                                </div>
                                <div class="tag-cloud">
                                    <ul>
                                        <li><a href="#">Warehouse</a></li>
                                        <li><a href="#">Ocean frieght</a></li>
                                        <li><a href="#">logistic</a></li>
                                        <li><a href="#">land transport</a></li>
                                        <li><a href="#">cost shipment</a></li>
                                        <li><a href="#">Air frieght</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
@endsection
