@extends('layouts.frontend-template')

@section('content')
    <div>
        <section class="inner-banner2 clearfix">
            <div class="container clearfix">
                <h2>ប្រធានបទ</h2>
            </div>
        </section>

        <section class="breadcumb-wrapper">
            <div class="container clearfix">
                <ul class="breadcumb">
                    <li><a href="/">ទំព័រដើម</a></li>
                    <li><span>ប្រធានបទ</span></li>
                </ul>
            </div>
        </section>


        <section class="diff-offer-wrapper diff-offer-wrapper3">
            <div class="container">
                <br/>

                <div class="single-sidebar-widget white-bg-border">
                    <div class="popular-post">
                                <div class="image-mobile col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <a href="/lesson/1">
                                        <img src="https://img1.exportersindia.com/product_images/bc-full/2019/7/4597960/desktop-computer-1561967368-4978714.jpeg" alt="Awesome Image" style="width: 100%;">
                                    </a>
                                </div>
                                <div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12"><a href="/lesson/1">
                                        <h3>ការប្រើប្រាស់កំព្យូទ័រឬឧបករណ៍របស់អ្នក</h3></a>
                                        <p>
                                            រៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេតរៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេតរៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេត។ នៅក្នុងវគ្គសិក្សានេះអ្នកនឹងរៀនអំពីមុខងារមូលដ្ឋានរបស់កុំព្យូទ័រនិងរបៀបប្រើវាដោយសុវត្ថិភាព។ អ្នកនឹងរៀនពីរបៀបបើកនិងបិទកុំព្យូទ័រនិងរបៀបផ្លាស់ប្តូរការកំណត់ដើម្បីឱ្យវាសមនឹងតម្រូវការរបស់អ្នក។ វគ្គសិក្សានេះក៏នឹងបង្រៀនអ្នកអំពីផ្នែកផ្សេងៗនៃកុំព្យូទ័រផងដែរ។ ការប្រើប្រាស់កុំព្យួទ័រអាចធ្វើអោយជីវិតរបស់អ្នកមានភាពងាយស្រួលនិងបើកឱកាសថ្មីៗជាច្រើនសម្រាប់អ្នក។ ការដឹងពីរបៀបតម្រៀបឯកសារនិងថតឯកសាររបស់អ្នកនឹងជួយអ្នកឱ្យប្រើកុំព្យូទ័ររបស់អ្នកកាន់តែមានប្រសិទ្ធភាព។ អ្នកត្រូវប្រាកដថាអ្នកអាចរកឃើញឯកសាររបស់អ្នកមិនថាវាជារូបថតឯកសារឬអ្វីផ្សេងទៀតទេ។
                                        </p>
                                    <span> <i class="fa fa-file-text" aria-hidden="true"></i> 6 ប្រធានបទ |  <i class="fa fa-tags" aria-hidden="true"></i> 7 resources</span>
                                    <br/>
                                    <a href="/lesson/1" class="btn btn3 btn-sm">ចាប់ផ្តើមប្រធានបទនេះ</a>
                                    <a href="#" class="btn btn-sm" data-toggle="modal" data-target="#exampleModalCenter">មើលវីដេអូ <i class="fa fa-play-circle" aria-hidden="true"></i></a>
                                </div>
                    </div>
                    <div class="single-sidebar-widget clearfix">
                        <div class="categories">
                            <ul>
                                <li><a href="/lesson/1"> 1, ការណែនាំអំពីកម្មវិធីការិយាល័យ</a></li>
                                <li><a href="/lesson/2"> 2, ការប្រើប្រាស់អេក្រង់ប៉ះ</a></li>
                                <li><a href="/lesson/3"> 3, ធ្វើច្រើនទៀតជាមួយអេក្រង់ប៉ះរបស់អ្នក</a></li>
                                <li><a href="/lesson/4"> 4, ការត្រួតពិនិត្យកម្រិតខ្ពស់បន្ថែមទៀត</a></li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="clearfix">
                    <div class="section_header2 common">
                        <h2>Resources</h2>
                    </div>
                    <p>
                        ស្វែងយល់បន្ថែមអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍របស់អ្នកជាមួយទំព័រពាក់ព័ន្ធទាំងនេះ ៖
                    </p>
                    <div class="accordian-area accordian-area-pad">
                        <div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">
                            <div class="panel panel-default">
                                <div id="headingOne" role="tab" class="panel-heading">
                                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed"><span>WE offer luxury service to our customer</span><i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                                </div>
                                <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingTwo" role="tab" class="panel-heading">
                                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed">WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                                </div>
                                <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingThree" role="tab" class="panel-heading">
                                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                                </div>
                                <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingFour" role="tab" class="panel-heading">
                                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour" class="collapsed">WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                                </div>
                                <div id="collapseFour" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 99999">
                <div class="modal-dialog modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="height: 45px;">
                            <h5 class="modal-title" id="exampleModalLongTitle">សិក្សាតាមរយៈវីដេអូ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -23px;">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="padding: 0px; margin-bottom: -5px;">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/ErLGE3-YZKc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">បិទ</button>
                        </div>
                    </div>
                </div>
            </div>


        </section>
    </div>
@endsection
