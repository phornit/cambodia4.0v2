@extends('layouts.frontend-template')

@section('content')
    <div>
        <section class="inner-banner2 clearfix">
            <div class="container clearfix">
                <h2>ប្រធានបទ</h2>
            </div>
        </section>

        <section class="breadcumb-wrapper">
            <div class="container clearfix">
                <ul class="breadcumb">
                    <li><a href="/">ទំព័រដើម</a></li>
                    <li><span>ប្រធានបទ</span></li>
                </ul>
            </div>
        </section>


        <section class="diff-offer-wrapper diff-offer-wrapper3">
            <div class="container">
                <br/>

                <div class="widget-search-blog" style="margin-bottom: 7px;">
                    <div class="row widget-inner single-sidebar-widget">
                        <form action="#" method="get" class="search-form">
                            <div class="input-group">
                                <input type="search" placeholder="បញ្ចូលពាក្យគន្លឹះស្វែងរក" class="form-control"><span class="input-group-addon">
                                <button type="submit"><i class="icon icon-Search"></i></button></span>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="single-sidebar-widget white-bg-border">
                    <div class="popular-post">
                        <div class="image-mobile col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <a href="/subjects/1">
                                <img src="https://img1.exportersindia.com/product_images/bc-full/2019/7/4597960/desktop-computer-1561967368-4978714.jpeg" alt="Awesome Image" style="width: 100%;">
                            </a>
                        </div>
                        <div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <a href="/subjects/1">
                                <h3>ការប្រើប្រាស់កំព្យូទ័រឬឧបករណ៍របស់អ្នក</h3>
                            </a>
                            <p>ស្វែងយល់អំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេត។ មុខវិជ្ជានេះរួមបញ្ចូលជំនាញមូលដ្ឋានដូចជាការប្រើប្រាស់ក្តារចុចកណ្តុរឬអេក្រង់ប៉ះ។.  </p><span>អានបន្តរ...</span>
                        </div>
                    </div>
                        <div class="accordian-area accordian-area-pad clearfix">
                            <div id="accordion[]" role="tablist" aria-multiselectable="true" class="panel-group">

                                <div class="panel panel-default">
                                    <div id="headingOne" role="tab" class="panel-heading">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                <span>ការប្រើប្រាស់ក្តារចុច</span><i class="fa fa-minus"></i><i class="fa fa-plus"></i><span class="span-right">មិនចាំបាច់ចុះឈ្មោះ</span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="panel-collapse collapse" aria-expanded="false">
                                        <div class="panel-body faq-content">
                                            <p>ស្វែងយល់អំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេត។ មុខវិជ្ជានេះរួមបញ្ចូលជំនាញមូលដ្ឋានដូចជាការប្រើប្រាស់ក្តារចុចកណ្តុរឬអេក្រង់ប៉ះ។.  </p><span><a href="#">អានបន្តរ...</a></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div id="headingTwo" role="tab" class="panel-heading">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed">
                                                ការប្រើប្រាស់ mouse <i class="fa fa-minus"></i><i class="fa fa-plus"></i>
                                                <span class="span-right">មិនចាំបាច់ចុះឈ្មោះ</span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body faq-content">
                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div id="headingThree" role="tab" class="panel-heading">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">
                                                WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i>
                                            </a></h4>
                                    </div>
                                    <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse" aria-expanded="false">
                                        <div class="panel-body faq-content">
                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div id="headingFour" role="tab" class="panel-heading">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour" class="collapsed">
                                                WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" role="tabpanel" aria-labelledby="headingFour" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body faq-content">
                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                </div>

                <div class="single-sidebar-widget white-bg-border">
                    <div class="popular-post">
                                <div class="image-mobile col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <a href="/subjects/1">
                                        <img src="https://blog.tutorming.com/hubfs/GettyImages-918365088.jpg" alt="Awesome Image" style="width: 100%;">
                                    </a>
                                </div>
                                <div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <a href="/subjects/1">
                                        <h3>ស្វែងរកការងារតាមអ៊ីនធឺណិត</h3>
                                    </a>
                                    <p>ការស្វែងរកនិងដាក់ពាក្យសុំការងារអាចជាការលំបាកប៉ុន្តែការរៀនសូត្រពីរបៀបស្វែងរកនិងដាក់ពាក្យសុំការងារតាមអ៊ិនធរណេតនឹងធ្វើឱ្យដំណើរការកាន់តែងាយស្រួលនិងបើកឱកាសកាន់តែច្រើនសម្រាប់អ្នក។  </p><span>អានបន្តរ...</span>
                                </div>
                    </div>

                    <div class="accordian-area accordian-area-pad clearfix">
                        <div id="accordion[]" role="tablist" aria-multiselectable="true" class="panel-group">

                            <div class="panel panel-default">
                                <div id="headingOne1" role="tab" class="panel-heading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1" class="collapsed">
                                            <span>ការប្រមាញ់ការងារតាមអ៊ីនធឺណិត</span><i class="fa fa-minus"></i><i class="fa fa-plus"></i><span class="span-right">មិនចាំបាច់ចុះឈ្មោះ</span></a></h4>
                                </div>
                                <div id="collapseOne1" role="tabpanel" aria-labelledby="headingOne1" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body faq-content">
                                        <p>ស្វែងយល់អំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេត។ មុខវិជ្ជានេះរួមបញ្ចូលជំនាញមូលដ្ឋានដូចជាការប្រើប្រាស់ក្តារចុចកណ្តុរឬអេក្រង់ប៉ះ។.  </p><span><a href="#">អានបន្តរ...</a></span>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="headingTwo1" role="tab" class="panel-heading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo1" class="collapsed">
                                            គេហទំព័រសេវាកម្មអាជីពថ្នាក់ជាតិ៖ វិធីណែនាំ <i class="fa fa-minus"></i><i class="fa fa-plus"></i>
                                            <span class="span-right">មិនចាំបាច់ចុះឈ្មោះ</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo1" role="tabpanel" aria-labelledby="headingTwo1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="headingThree1" role="tab" class="panel-heading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree1" aria-expanded="false" aria-controls="collapseThree1" class="collapsed">
                                            ការងារនិងបទសម្ភាសន៍<i class="fa fa-minus"></i><i class="fa fa-plus"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree1" role="tabpanel" aria-labelledby="headingThree1" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="single-sidebar-widget white-bg-border">
                    <div class="popular-post">
                                <div class="image-mobile col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <a href="/subjects/1"><img src="https://www.theyucatantimes.com/wp-content/uploads/2019/06/trees-growing-on-coins-678x381.jpg" alt="Awesome Image" style="width: 100%;"></a>
                                </div>
                                <div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <a href="/subjects/1">
                                        <h3>គ្រប់គ្រងប្រាក់របស់អ្នកតាមអ៊ីនធឺណិត</h3>
                                    </a>
                                    <p>ហិរញ្ញវត្ថុអាចជាល្បិចកលប៉ុន្តែមុខវិជ្ជានេះនឹងជួយអ្នកឱ្យរៀនពីរបៀបរៀបចំថវិកាធនាគារនិងទិញទំនិញតាមអ៊ិនធរណេតតាមរបៀបដែលមានសុវត្ថិភាពនិងងាយស្រួល។  </p><span>អានបន្តរ...</span>
                                </div>
                    </div>
                    <div class="accordian-area accordian-area-pad clearfix">
                        <div id="accordion[]" role="tablist" aria-multiselectable="true" class="panel-group">

                            <div class="panel panel-default">
                                <div id="headingOne3" role="tab" class="panel-heading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3" class="collapsed">
                                            <span>ការប្រើប្រាស់ក្តារចុច</span><i class="fa fa-minus"></i><i class="fa fa-plus"></i><span class="span-right">មិនចាំបាច់ចុះឈ្មោះ</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne3" role="tabpanel" aria-labelledby="headingOne3" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body faq-content">
                                        <p>ស្វែងយល់អំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេត។ មុខវិជ្ជានេះរួមបញ្ចូលជំនាញមូលដ្ឋានដូចជាការប្រើប្រាស់ក្តារចុចកណ្តុរឬអេក្រង់ប៉ះ។.  </p><span><a href="#">អានបន្តរ...</a></span>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="headingTwo3" role="tab" class="panel-heading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo3" aria-expanded="false" aria-controls="collapseTwo3" class="collapsed">
                                            ការប្រើប្រាស់ mouse <i class="fa fa-minus"></i><i class="fa fa-plus"></i>
                                            <span class="span-right">មិនចាំបាច់ចុះឈ្មោះ</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo3" role="tabpanel" aria-labelledby="headingTwo3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="headingThree3" role="tab" class="panel-heading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3" class="collapsed">
                                            WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i>
                                        </a></h4>
                                </div>
                                <div id="collapseThree3" role="tabpanel" aria-labelledby="headingThree3" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="headingFour3" role="tab" class="panel-heading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour3" aria-expanded="false" aria-controls="collapseFour3" class="collapsed">
                                            WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour3" role="tabpanel" aria-labelledby="headingFour3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                </div>

        </section>
    </div>
@endsection
