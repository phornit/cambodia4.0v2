@extends('layouts.frontend-template')

@section('content')
    <div>
        <section class="inner-banner2 clearfix">
            <div class="container clearfix">
                <h2>ប្រធានបទ</h2>
            </div>
        </section>

        <section class="breadcumb-wrapper">
            <div class="container clearfix">
                <ul class="breadcumb">
                    <li><a href="/">ទំព័រដើម</a></li>
                    <li><span>ប្រធានបទ</span></li>
                </ul>
            </div>
        </section>


        <section class="diff-offer-wrapper diff-offer-wrapper3">
            <div class="container">
                <br/>

                <div class="single-sidebar-widget" style="border: 1px solid #E6E6E6; padding: 20px; background-color: white;">
                    <div class="popular-post" style="border-bottom: 1px solid #E6E6E6; padding-bottom: 20px;">
                        <ul>
                            <li class="img-cap-effect">
                                <div class="img-box"><a href="/lesson/1"><img src="{{ asset('frontend/images/news/popular-post1.jpg')}}" alt="Awesome Image"></a></div>
                                <div class="content"><a href="/lesson/1">
                                        <h4>ការប្រើប្រាស់កំព្យូទ័រឬឧបករណ៍របស់អ្នក</h4></a>
                                        <p>
                                            រៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេតរៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេតរៀនអំពីការប្រើប្រាស់កុំព្យូទ័រឬឧបករណ៍ចល័តដូចជាទូរស័ព្ទឬថេប្លេត។ នៅក្នុងវគ្គសិក្សានេះអ្នកនឹងរៀនអំពីមុខងារមូលដ្ឋានរបស់កុំព្យូទ័រនិងរបៀបប្រើវាដោយសុវត្ថិភាព។ អ្នកនឹងរៀនពីរបៀបបើកនិងបិទកុំព្យូទ័រនិងរបៀបផ្លាស់ប្តូរការកំណត់ដើម្បីឱ្យវាសមនឹងតម្រូវការរបស់អ្នក។ វគ្គសិក្សានេះក៏នឹងបង្រៀនអ្នកអំពីផ្នែកផ្សេងៗនៃកុំព្យូទ័រផងដែរ។ ការប្រើប្រាស់កុំព្យួទ័រអាចធ្វើអោយជីវិតរបស់អ្នកមានភាពងាយស្រួលនិងបើកឱកាសថ្មីៗជាច្រើនសម្រាប់អ្នក។ ការដឹងពីរបៀបតម្រៀបឯកសារនិងថតឯកសាររបស់អ្នកនឹងជួយអ្នកឱ្យប្រើកុំព្យូទ័ររបស់អ្នកកាន់តែមានប្រសិទ្ធភាព។ អ្នកត្រូវប្រាកដថាអ្នកអាចរកឃើញឯកសាររបស់អ្នកមិនថាវាជារូបថតឯកសារឬអ្វីផ្សេងទៀតទេ។
                                        </p>
                                    <span> <i class="fa fa-file-text" aria-hidden="true"></i> 6 topics |  <i class="fa fa-tags" aria-hidden="true"></i> 7 resources</span>
                                    <br/>
                                    <a href="#" class="btn btn3 btn-sm">Start This Subject</a>
                                    <a href="#" class="btn btn-sm">Watch Video <i class="fa fa-play-circle" aria-hidden="true"></i></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="single-sidebar-widget">
                        <div class="categories">
                            <ul>
                                <li><a href="/lesson/1"> 1, Chemistry of Petrolium Filteration</a></li>
                                <li><a href="/lesson/2"> 2, Alluminium Strength Depends</a></li>
                                <li><a href="/lesson/3"> 3, Solar Panel Working Process</a></li>
                                <li><a href="/lesson/4"> 4, Advanced Materials Innovation</a></li>
                                <li><a href="/lesson/5"> 5, Metal Manufacturing Process</a></li>
                                <li><a href="/lesson/6"> 6, Construction and Architecture</a></li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="clearfix">
                    <div class="section_header2 common">
                        <h2>Resources</h2>
                    </div>
                    <p>
                        Learn more about Using your computer or device with these related pages:
                    </p>
                    <div class="accordian-area accordian-area-pad">
                        <div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">
                            <div class="panel panel-default">
                                <div id="headingOne" role="tab" class="panel-heading">
                                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed"><span>WE offer luxury service to our customer</span><i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                                </div>
                                <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingTwo" role="tab" class="panel-heading">
                                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed">WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                                </div>
                                <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingThree" role="tab" class="panel-heading">
                                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                                </div>
                                <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingFour" role="tab" class="panel-heading">
                                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour" class="collapsed">WE offer luxury service to our customer<i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                                </div>
                                <div id="collapseFour" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body faq-content">
                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
@endsection
