import Dashboard from './components/dashboard.vue'

import UserList from './components/auth/users/user-list.vue'
import UserAdd from './components/auth/users/user-add.vue'
import UserEdit from './components/auth/users/user-edit.vue'
import Profile from './components/auth/users/user-profile.vue'
import MainMenu from './components/backend/main_menu/main-menu.vue'

import RoleList from './components/auth/roles/role-list.vue'

import Categories from './components/backend/content_management/categories-list.vue'
import ContentList from './components/backend/content_management/content-list.vue'
import ContentAdd from './components/backend/content_management/content-add.vue'
import ContentEdit from './components/backend/content_management/content-edit.vue'

import YoungMathScienceList from './components/backend/member_registered/young-math-science-list.vue'
import StartUpClub from './components/backend/member_registered/startup-club.vue'
import DigitalTechnologyScience from './components/backend/member_registered/digital-technology-science.vue'
import CambodiaCenterLegalEntity from './components/backend/member_registered/cambodai-legal-entity.vue'
import CambodiaCenterPhysicalPerson from './components/backend/member_registered/cambodia-physical-person.vue'

import fileUpload from './components/corebackend/fileUpload.vue'
import file_upload from './components/corebackend/file-upload.vue'
import resizeImagePost from './components/corebackend/resizeImagePost.vue'

export const routes = [
    {
        name:'dashboard',
        path:'/dashboard',
        component:Dashboard
    },
    {
        name:'UserList',
        path:'/user-List',
        component:UserList
    },
    {
        name:'UserAdd',
        path:'/user-add',
        component:UserAdd
    },
    {
        name:'MainMenu',
        path:'/main-menu',
        component:MainMenu
    },
    {
        name:'UserEdit',
        path:'/user-edit/:userid',
        component:UserEdit
    },
    {
        name:'Profile',
        path:'/profile',
        component:Profile
    },
    {
        name:'RoleList',
        path:'/role-list',
        component:RoleList
    },
    {
        name:'Categories',
        path:'/categories-list',
        component:Categories
    },
    {
        name:'ContentList',
        path:'/content-list',
        component:ContentList
    },
    {
        name:'ContentAdd',
        path:'/content-add',
        component:ContentAdd
    },
    {
        name:'ContentEdit',
        path:'/content-edit/:contentid',
        component:ContentEdit
    },
    {
        name:'YoungMathScienceList',
        path:'/young-math-science-list',
        component: YoungMathScienceList
    },
    {
        name:'StartUpClub',
        path:'/startup-club',
        component: StartUpClub
    },
    {
        name:'DigitalTechnologyScience',
        path:'/digital-technology-science',
        component: DigitalTechnologyScience
    },
    {
        name:'CambodiaCenterLegalEntity',
        path:'/cambodia-center-legal-entity',
        component: CambodiaCenterLegalEntity
    },
    {
        name:'CambodiaCenterPhysicalPerson',
        path:'/cambodia-center-physical-person',
        component: CambodiaCenterPhysicalPerson
    }
    ,{
        name:'fileUpload',
        path:'/file-upload',
        component: fileUpload
    }
    ,{
        name:'file_upload',
        path:'/file_upload',
        component: file_upload
    }
    ,{
        name:'resizeImagePost',
        path:'/resizeImagePost',
        component: resizeImagePost
    }
];


